pragma solidity "0.5.4";

contract HashStore {
    struct HashInfo {
        address sender;
        uint256[] blockOfHash;
        string hash;
        uint256 createdAt;
    }

    // maps hash string to its block number at storage time
    mapping(string => HashInfo) hashInfos;

    event ContractCreated(string message);
    event HashStored(
        address sender,
        string fileHash,
        uint256[] blockNumber,
        uint256 createdAt
    );

    constructor() public {
        emit ContractCreated("Contract Created");
    }

    /**
     * @dev stores and maps a specified fileHash to its blockNumber of storage time
     * @param fileHash string represents hash of file
     * @return boolean represents success or failure
     */
    function storeHash(string memory fileHash) public {
        HashInfo storage hashInfo = hashInfos[fileHash];

        hashInfo.sender = msg.sender;
        hashInfo.hash = fileHash;
        hashInfo.blockOfHash.push(block.number);
        hashInfo.createdAt = now;

        emit HashStored(
            hashInfo.sender,
            hashInfo.hash,
            hashInfo.blockOfHash,
            hashInfo.createdAt
        );
    }

    /**
     * @dev verifies whether a specified hash stored or not
     * @param fileHash string represents hash of file
     * @return uint256[] an array of block numbers of storage time if stored
     */
    function verifyHash(string memory fileHash)
        public
        view
        returns (
            address sender,
            string memory hash,
            uint256[] memory blockNumbers,
            uint256 createdAt
        )
    {
        HashInfo memory hashInfo = hashInfos[fileHash];

        return (
            hashInfo.sender,
            hashInfo.hash,
            hashInfo.blockOfHash,
            hashInfo.createdAt
        );
    }
}
