module.exports = {
  networks: {
    shasta: {
      privateKey: '8441f32772af37e2145b95b0a5262943e5286e8c93180b2388195d323b496ccd',
      userFeePercentage: 100,
      feeLimit: 1e8,
      fullHost: 'https://api.shasta.trongrid.io',
      network_id: '2',
    },

    development: {
      from: 'some address',
      privateKey: 'some private key',
      consume_user_resource_percent: 30,
      fee_limit: 100000000,
      fullNode: 'https://api.trongrid.io',
      solidityNode: 'https://api.trongrid.io',
      eventServer: 'it is optional',
      network_id: '*', // Match any network id
    },
    production: {
      from: 'some other address',
      privateKey: 'some other private key',
      consume_user_resource_percent: 30,
      fee_limit: 100000000,
      fullNode: 'https://api.trongrid.io',
      solidityNode: 'https://api.trongrid.io',
      eventServer: 'it is optional',
      network_id: '*', // Match any network id
    },
    compilers: {
      solc: {
        version: '0.5.4',
      },
    },
  },
};
